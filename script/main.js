/**
 * Created by valter on 01.05.16.
 */

var switchesLeft = 0;

var gamesWon = 0;
var gamesLost = 0;

var schemes;

function loadScheme(scheme) {
    var i;
    for (i = 0; i < scheme.wires.length; i++) {
        var wire = scheme.wires[i];
        if (wire.on) {
            mainWireIndexes.push(i);
        }
        if (!wire.cnt) {
            wire.cnt = 1;
        }

        buildWire(wire.left, wire.top, wire.cnt, wire.on, wire.vertical, scheme.measure);
    }

    for (i = 0; i < scheme.switches.length; i++) {
        var s = scheme.switches[i];
        buildSwitch(s.left, s.top, s.on, s.vertical, scheme.measure);
    }

    for (i = 0; i < scheme.bombs.length; i++) {
        var bomb = scheme.bombs[i];
        buildBomb(bomb.left, bomb.top, bomb.on, bomb.vertical, scheme.measure);
    }

    for (i = 0; i < scheme.bulbs.length; i++) {
        var bulb = scheme.bulbs[i];
        buildBulb(bulb.left, bulb.top, bulb.on, bulb.vertical, scheme.measure);
    }

    switchesToWiresConnections = scheme.switchesToWires;
    wiresToSwitchesConnections = scheme.wiresToSwitches;
    wiresToBombsConnections = scheme.wiresToBombs;
    wiresToBulbsConnections = scheme.wiresToBulbs;

    switchesLeft = scheme.movesCount;
}

function changeSwitchState() {
    if (this.classList.contains('on')) {
        changeState(this, false);
    } else if (this.classList.contains('off')) {
        changeState(this, true);
    }

    for (var j = 0; j < wires.length; j++) {
        changeWire(j, false);
    }

    for (var k = 0; k < bulbs.length; k++) {
        changeBulb(k, false);
    }

    for (var n = 0; n < mainWireIndexes.length; n++) {
        changeWire(mainWireIndexes[n], true);
    }

    for (var m = 0; m < mainWireIndexes.length; m++) {
        passElectricity(mainWireIndexes[m], []);
    }

    switchesLeft--;
    showCurrentScore();

    var finish = false;
    var win = true;

    if (switchesLeft == 0) {
        finish = true;

        for (var i = 0; i < bulbs.length; i++) {
            var bulb = bulbs[i];
            if (bulb.classList.contains('off')) {
                win = false;
                break;
            }
        }
    }

    for (var l = 0; l < bombs.length; l++) {
        var bomb = bombs[l];
        if (bomb.classList.contains('on')) {
            finish = true;
            win = false;
            break;
        }
    }

    if (finish) {
        finishGame(win);
    }
}

function passElectricity(wireInd, wiresVisited) {
    var contains = false;
    for (var i = 0; i < wiresVisited.length; i++) {
        if (wiresVisited[i] == wireInd) {
            contains = true;
            break;
        }
    }
    if (contains) {
        return;
    }
    wiresVisited.push(wireInd);
    changeWire(wireInd, true);

    for (var k = 0; k < wiresToBulbsConnections.length; k++) {
        var bulbConnection = wiresToBulbsConnections[k];
        if (bulbConnection[0] == wireInd) {
            changeBulb(bulbConnection[1], true);
        }
    }

    for (var m = 0; m < wiresToBombsConnections.length; m++) {
        var bombConnection = wiresToBombsConnections[m];
        if (bombConnection[0] == wireInd) {
            changeBomb(bombConnection[1], true);
        }
    }

    for (var j = 0; j < wiresToSwitchesConnections.length; j++) {
        var switchConnection = wiresToSwitchesConnections[j];
        var switchInd = switchConnection[1];
        if (switchConnection[0] == wireInd && switches[switchInd].classList.contains('on')) {
            for (var l = 0; l < switchesToWiresConnections.length; l++) {
                var switchToWireConnection = switchesToWiresConnections[l];
                if (switchToWireConnection[0] == switchInd) {
                    passElectricity(switchToWireConnection[1], wiresVisited);
                }
            }
        }
    }
}

function changeWire(ind, on) {
    var wire = wires[ind];
    for (var i = 0; i < wire.length; i++) {
        changeState(wire[i], on);
    }
}

function changeBulb(ind, on) {
    changeState(bulbs[ind], on);
}

function changeBomb(ind, on) {
    changeState(bombs[ind], on);
}

function showCurrentScore() {
    var score = document.getElementById('score-current');
    score.innerHTML = 'Switches left: ' + switchesLeft;
}

function showTotalScore() {
    var score = document.getElementById('score-total');
    score.innerHTML = 'Games won: ' + gamesWon + '<br/>Games lost: ' + gamesLost;
}

function giveUp() {
    finishGame(false);
}

function startToPlay() {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (xhttp.readyState == 4 && xhttp.status == 200) {
            schemes = JSON.parse(xhttp.responseText).schemes;
            newGame();
        }
    };
    xhttp.open('GET', 'script/schemes.json', true);
    xhttp.send();
}

function newGame() {
    initScheme();
    loadScheme(schemes[(gamesWon + gamesLost) % schemes.length]);
    showCurrentScore();
    showTotalScore();
}

function finishGame(win) {
    setTimeout(function() {
        if (win) {
            alert('You won!');
            gamesWon++;
        } else {
            alert('You lost!');
            gamesLost++;
        }

        newGame();
    }, 500);
}
