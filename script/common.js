var scheme = document.getElementById('scheme');

var mainWireIndexes = [];

var switches = [];
var wires = [];
var bulbs = [];
var bombs = [];

var switchesToWiresConnections = [];
var wiresToSwitchesConnections = [];
var wiresToBulbsConnections = [];
var wiresToBombsConnections = [];

var wireHeightEm = 2;
var wireWidthEm = 2;

var wireHeightPx = convertEm(wireHeightEm, document.getElementById('em-converter'));
var wireWidthPx = convertEm(wireWidthEm, document.getElementById('em-converter'));

function getElementFontSize( context ) {
    return parseFloat(
        getComputedStyle(
            context
            || document.documentElement
        ).fontSize
    );
}

function convertEm(value, context) {
    return value * getElementFontSize(context);
}


function initScheme() {
    scheme.innerHTML = '';

    mainWireIndexes =[];

    switches = [];
    wires = [];
    bulbs = [];
    bombs = [];

    switchesToWiresConnections = [];
    wiresToSwitchesConnections = [];
    wiresToBulbsConnections = [];
    wiresToBombsConnections = [];
}

function buildBulb(left, top, on, vertical, measure) {
    var div = buildElement(left, top, 'bulb', on, vertical, measure);
    bulbs.push(div);
}

function buildBomb(left, top, on, vertical, measure) {
    var div = buildElement(left, top, 'bomb', on, vertical, measure);
    bombs.push(div);
}

function buildSwitch(left, top, on, vertical, measure) {
    var div = buildElement(left, top, 'switch', on, vertical, measure);
    div.onclick = changeSwitchState;
    div.id = 'switch-' + switches.length;
    switches.push(div);
}

function buildWire(left, top, cnt, on, vertical, measure) {
    var wire = [];
    for (var i = 0; i < cnt; i++) {
        var height;
        var width;
        if (measure == 'em') {
            height = wireHeightEm;
            width = wireWidthEm;
        } else if (measure == 'px') {
            height = wireHeightPx;
            width = wireWidthPx;
        }

        var wireElement;
        if (vertical) {
            wireElement = buildElement(left, (top + (i * height)), 'wire', on, vertical, measure);
        } else {
            wireElement = buildElement((left + (i * width)), top, 'wire', on, vertical, measure);
        }
        wire.push(wireElement);
    }
    wires.push(wire);
}

function buildElement(left, top, type, on, vertical, measure) {
    var div = document.createElement('div');
    div.classList.add(type);
    if (on) {
        div.classList.add('on');
    } else {
        div.classList.add('off');
    }

    if (vertical) {
        div.classList.add('vertical');
    }

    div.style.top = top + measure;
    div.style.left = left + measure;

    scheme.appendChild(div);
    return div;
}

function changeState(element, on) {
    var classes = element.classList;
    if (!on) {
        classes.remove('on');
        classes.add('off');
    } else {
        classes.remove('off');
        classes.add('on');
    }
}

function changeVertical(element, vertical) {
    var classes = element.classList;
    if (!vertical && classes.contains('vertical')) {
        classes.remove('vertical');
    } else if (!classes.contains('vertical')) {
        classes.add('vertical');
    }
}