scheme.onclick = deselectComponents;

function addBulb() {
    var div = addComponent();
    div.classList.add('bulb');
    div.classList.add('off');
    scheme.appendChild(div);
    showRulers(div);

    div.id = 'bulb-' + bulbs.length;
    bulbs.push(div);
}

function addSwitch() {
    var div = addComponent();
    div.classList.add('switch');
    div.classList.add('off');
    scheme.appendChild(div);
    showRulers(div);

    div.id = 'switch-' + switches.length;
    switches.push(div);
}

function addBomb() {
    var div = addComponent();
    div.classList.add('bomb');
    div.classList.add('off');
    scheme.appendChild(div);
    showRulers(div);

    div.id = 'bomb-' + bombs.length;
    bombs.push(div);
}

function addWire() {
    var div = addComponent();
    div.classList.add('wire');
    div.classList.add('off');
    scheme.appendChild(div);
    showRulers(div);

    div.id = 'wire-' + wires.length;
    wires.push(div);
}

function removeBulb(e) {
    var parent = this.parentNode;
    removeFromArray(parent, bulbs);
    removeRulers(parent);
    parent.parentNode.removeChild(parent);
    clearCurrentParams();
    e.stopPropagation();
}

function removeSwitch(e) {
    var parent = this.parentNode;
    removeFromArray(parent, switches);
    removeRulers(parent);
    parent.parentNode.removeChild(parent);
    clearCurrentParams();
    e.stopPropagation();
}

function removeBomb(e) {
    var parent = this.parentNode;
    removeFromArray(parent, bombs);
    removeRulers(parent);
    parent.parentNode.removeChild(parent);
    clearCurrentParams();
    e.stopPropagation();
}

function removeWire(e) {
    var parent = this.parentNode;
    removeFromArray(parent, wires);
    removeRulers(parent);
    parent.parentNode.removeChild(parent);
    clearCurrentParams();
    e.stopPropagation();
}

function removeRulers(element) {
    var rulers = element.getElementsByClassName('ruler');
    for (var i = 0; i < rulers.length; i++) {
        var ruler = rulers[i];
        if (ruler.connectionId) {
            removeConnection(ruler.connectionId);
        }
    }
}

function removeFromArray(e, arr) {
    var index = arr.indexOf(e);
    arr.splice(index, 1);
}

var connectionId = 1;

function addComponent() {
    var div = document.createElement('div');

    div.onmousedown = function(e) {
        selectComponent(div);

        var coords = getCoords(div);
        var shiftX = e.pageX - coords.left + 1;
        var shiftY = e.pageY - coords.top + 1;

        div.style.zIndex = 1000;

        div.block = false;

        moveAt(e);

        function moveAt(e) {
            if (div.block) {
                return;
            }

            var hasRulers = false;
            for (var j = 0; j < div.childNodes.length; j++) {
                if (div.childNodes[j].className == 'ruler') {
                    if (div.childNodes[j].connectionId) {
                        hasRulers = true;
                    }
                }
            }

            if (hasRulers) {
                return;
            }

            var ruler = null;
            var connection = null;

            for (var i = 0; i < div.childNodes.length; i++) {
                if (div.childNodes[i].className == "ruler") {
                    connection = findClosestRuler(div.childNodes[i]);
                    if (connection) {
                        ruler = div.childNodes[i];
                        break;
                    }
                }
            }

            if (connection != null && !checkCanConnect(div, connection.parentNode)) {
                connection = null;
            }

            if (connection == null) {
                var lOffset = 0;
                var tOffset = 0;
                if (div.classList.contains('vertical')) {
                    var len = div.offsetWidth - div.offsetHeight;
                    lOffset = -len / 2;
                    tOffset = len / 2;
                }
                div.style.left = lOffset + e.pageX - shiftX - scheme.offsetLeft + 'px';
                div.style.top = tOffset + e.pageY - shiftY - scheme.offsetTop + 'px';
            } else {
                console.log('rule ' + connectionId + ' ' + div.style.left + ' ' + div.style.top);

                var divL = getCoords(connection).left - getCoords(ruler).left;
                var divT = getCoords(connection).top - getCoords(ruler).top;

                var l = parseInt(div.style.left.substring(0, div.style.left.length - 2));
                var t = parseInt(div.style.top.substring(0, div.style.top.length - 2));
                div.style.left = (l + divL) + 'px';
                div.style.top = (t + divT) + 'px';
            }

            addConnections(div);
        }

        scheme.onmousemove = function(e) {
            moveAt(e);
        };

        div.onmousemove = function(e) {
            moveAt(e);
            e.stopPropagation();
        };

        scheme.onmouseup = function() {
            scheme.onmousemove = null;
            div.onmouseup = null;
            div.onmousemove = null;
        };

        div.onmouseup = function() {
            scheme.onmousemove = null;
            div.onmouseup = null;
            div.onmousemove = null;
        };
    };

    div.ondragstart = function() {
        return false;
    };

    div.onclick = function() {
        event.stopPropagation();
    };

    div.classList.add('component');
    div.style.position = 'absolute';
    div.style.top = 0;
    div.style.left = 0;

    return div;
}

function addConnections(div) {
    var connections = [];
    for (var i = 0; i < div.childNodes.length; i++) {
        if (div.childNodes[i].className == "ruler") {
            var ruler = div.childNodes[i];
            var connection = findClosestRuler(div.childNodes[i], 2);
            if (connection) {
                connections.push([ruler, connection]);
            }
        }
    }

    for (var j = 0; j < connections.length; j++) {
        console.log('rule ' + connectionId + ' ' + div.style.left + ' ' + div.style.top);

        addConnection(connections[j][0], connections[j][1], connectionId);
        connectionId++;
    }

    if (connections.length > 0) {
        div.block = true;

        setTimeout(function () {
            div.block = false;
        }, 200);
    }
}

var colors = [
    '#ff0000',
    '#00ff00',
    '#0000ff',

    '#111111',

    '#550000',
    '#005500',
    '#000055',

    '#555555'
];

function removeConnection(connectionId) {
    var rulers = document.getElementsByClassName('ruler');
    for (var j = 0; j < rulers.length; j++) {
        var ruler = rulers[j];
        if (ruler.connectionId && ruler.connectionId == connectionId) {
            ruler.connectionId = undefined;
            ruler.style.borderColor = 'white';
        }
    }
}

function addConnection(el1, el2, connectionId) {
    el1.connectionId = connectionId;
    el2.connectionId = connectionId;

    el1.style.borderColor = colors[connectionId % colors.length];
    el2.style.borderColor = colors[connectionId % colors.length];
}

function checkCanConnect(element1, element2) {
    var wire;
    if (element1.classList.contains('wire')) {
        wire = element1;
    }

    if (element2.classList.contains('wire')) {
        if (wire) {
            return false;
        }
        wire = element2;
    }

    return wire;
}

document.getElementById('download').onclick = function (e) {
    e.preventDefault();
    var data = buildJson();
    if (!data) {
        return;
    }
    window.open('data:text/json;charset=utf-8,' + encodeURIComponent(JSON.stringify(data)));
};

function buildJson() {
    if (!document.getElementById('name').value || !document.getElementById('moves-count').value) {
        alert('Fill global params!');
        return;
    }

    buildConnections();
    return {
        name: document.getElementById('name').value,
        movesCount: document.getElementById('moves-count').value,
        measure: 'px',
        wires: replaceDivs(wires),
        switches: replaceDivs(switches),
        bombs: replaceDivs(bombs),
        bulbs: replaceDivs(bulbs),
        switchesToWires: switchesToWiresConnections,
        wiresToSwitches: wiresToSwitchesConnections,
        wiresToBombs: wiresToBombsConnections,
        wiresToBulbs: wiresToBulbsConnections
    }
}

function replaceDivs(arr) {
    var result = [];
    for (var i = 0; i < arr.length; i++) {
        var obj = {};
        if (arr[i].cnt) {
            obj.cnt = arr[i].cnt;
        } else if (arr[i].classList.contains('wire')) {
            obj.cnt = 1;
        }

        obj.on = arr[i].classList.contains('on');
        obj.vertical = arr[i].classList.contains('vertical');

        var lOffset = 0;
        var tOffset = 0;
        if (arr[i].classList.contains('wire') && arr[i].classList.contains('vertical')) {
            var len = arr[i].offsetWidth - arr[i].offsetHeight;
            lOffset = -len / 2;
            tOffset = len / 2;
        }

        obj.left = parseInt(arr[i].style.left.substring(0, arr[i].style.left.length - 2)) - lOffset;
        obj.top = parseInt(arr[i].style.top.substring(0, arr[i].style.top.length - 2)) - tOffset;

        result.push(obj);
    }
    return result;
}

function buildConnections() {
    var inds = [];

    var rulers = document.getElementsByClassName('ruler');
    for (var j = 0; j < rulers.length; j++) {
        var ruler1 = rulers[j];
        if (ruler1.connectionId) {

            for (var i = 0; i < rulers.length; i++) {
                var ruler2 = rulers[i];
                if (i != j && ruler2.connectionId && ruler1.connectionId == ruler2.connectionId) {
                    var found = false;
                    for (var k = 0; k < inds.length; k++) {
                        if (inds[k] == ruler1.connectionId) {
                            found = true;
                            break;
                        }
                    }
                    if (found) {
                        continue;
                    }

                    inds.push(ruler1.connectionId);

                    buildConnection(ruler1, ruler2);
                }
            }

        }
    }
}

function buildConnection(el1, el2) {
    var element1 = el1.parentNode;
    var element2 = el2.parentNode;
    if (!checkCanConnect(element1, element2)) {
        return;
    }

    var wire;
    var switchEl;
    var bulbEl;
    var bombEl;
    if (element1.classList.contains('wire')) {
        wire = element1;
        if (element2.classList.contains('switch')) {
            switchEl = element2;
        } else if (element2.classList.contains('bomb')) {
            bombEl = element2;
        } else if (element2.classList.contains('bulb')) {
            bulbEl = element2;
        }
    }

    if (element2.classList.contains('wire')) {
        if (wire) {
            return false;
        }
        wire = element2;
        if (element1.classList.contains('switch')) {
            switchEl = element1;
        } else if (element1.classList.contains('bomb')) {
            bombEl = element1;
        } else if (element1.classList.contains('bulb')) {
            bulbEl = element1;
        }
    }

    var wireInd = getInd(wire, wires);
    var switchInd = getInd(switchEl, switches);
    var bombInd = getInd(bombEl, bombs);
    var bulbInd = getInd(bulbEl, bulbs);

    if (switchEl) {
        switchesToWiresConnections.push([switchInd, wireInd]);
        wiresToSwitchesConnections.push([wireInd, switchInd]);
    } else if (bulbEl) {
        wiresToBulbsConnections.push([wireInd, bulbInd]);
    } else if (bombEl) {
        wiresToBombsConnections.push([wireInd, bombInd]);
    }
}

function getInd(e, arr) {
    var ind;
    for (var i = 0; i < arr.length; i++) {
        if (arr[i] == e) {
            ind = i;
            break;
        }
    }
    return ind;
}

function findClosestRuler(ruler, eps) {
    var rulers = document.getElementsByClassName('ruler');

    for (var i = 0; i < rulers.length; i++) {
        var current = rulers[i];
        if (current != ruler && isClose(ruler, current, eps)) {
            return current;
        }
    }
    return null;
}

function isClose(ruler1, ruler2, eps) {
    if (!eps) {
        eps = 10;
    }

    var coords1 = getCoords(ruler1);
    var coords2 = getCoords(ruler2);

    var l = (coords2.left - coords1.left) * (coords2.left - coords1.left);
    var t = (coords2.top - coords1.top) * (coords2.top - coords1.top);
    return Math.sqrt(l + t) < eps;
}

function getCoords(elem) {
    var box = elem.getBoundingClientRect();

    var body = document.body;
    var docEl = document.documentElement;

    var scrollTop = window.pageYOffset || docEl.scrollTop || body.scrollTop;
    var scrollLeft = window.pageXOffset || docEl.scrollLeft || body.scrollLeft;

    var clientTop = docEl.clientTop || body.clientTop || 0;
    var clientLeft = docEl.clientLeft || body.clientLeft || 0;

    var top  = box.top +  scrollTop - clientTop;
    var left = box.left + scrollLeft - clientLeft;

    return { top: Math.round(top), left: Math.round(left) };
}


function showWireParams(element) {
    var params = document.getElementById('params-current');
    clearCurrentParams();
    addOnAndVertical(element, params);

    var input = document.createElement('input');
    input.id = 'param-cnt';
    input.type = 'text';
    input.placeholder = 'cnt';

    if (element.cnt) {
        input.value = element.cnt;
    }

    input.onchange = function() {
        var cnt = parseInt(input.value);
        element.style.width = (wireWidthEm * cnt) + 'em';
        element.cnt = cnt;
    };
    params.appendChild(input);
}

function showSwitchParams(element) {
    var params = document.getElementById('params-current');
    clearCurrentParams();
    addOnAndVertical(element, params);
}

function addOnAndVertical(element, params) {
    var input = document.createElement('input');
    input.id = 'param-on';
    input.type = 'checkbox';

    if (element.classList.contains('on')) {
        input.checked = true;
    }

    input.onchange = function() {
        changeState(element, input.checked);
    };
    var label = document.createElement('label');
    label.for = 'param-on';
    label.innerHTML = 'on';
    params.appendChild(input);
    params.appendChild(label);

    var input2 = document.createElement('input');
    input2.id = 'param-vertical';
    input2.type = 'checkbox';
    if (element.classList.contains('vertical')) {
        input2.checked = true;
    }

    input2.onchange = function() {
        changeVertical(element, input2.checked);
    };
    var label2 = document.createElement('label');
    label2.for = 'param-vertical';
    label2.innerHTML = 'vertical';
    params.appendChild(input2);
    params.appendChild(label2);
}

function showBombParams(element) {
    var params = document.getElementById('params-current');
    clearCurrentParams();
}

function showBulbParams(element) {
    var params = document.getElementById('params-current');
    clearCurrentParams();
}

function clearCurrentParams() {
    var params = document.getElementById('params-current');
    params.innerHTML = '<div>Current params</div>';
}


function showWireConnections(element) {
    var div = document.createElement('div');
    div.classList.add('ruler');
    div.style.top = 0.75 + 'em';
    div.style.left = 0 + 'em';
    element.appendChild(div);

    div = document.createElement('div');
    div.classList.add('ruler');
    div.style.top = 0.75 + 'em';
    div.style.right = 0 + 'em';
    element.appendChild(div);
}

function showBombConnections(element) {
    showConnections(element, [[2.75, 2.75]]);
}

function showSwitchConnections(element) {
    showConnections(element, [[1.25, 0], [1.25, 1.5], [0, 0.75], [2.5, 0.75]]);
}

function showBulbConnections(element) {
    showConnections(element, [[6.5, 2.75]]);
}

function showConnections(element, coords) {
    for (var i = 0; i < coords.length; i++) {
        var div = document.createElement('div');
        div.classList.add('ruler');
        div.style.top = coords[i][0] + 'em';
        div.style.left = coords[i][1] + 'em';
        element.appendChild(div);
    }
}

function showDeleteSign(element, callback) {
    var div = document.createElement('div');
    div.classList.add('delete-sign');
    div.style.top = 0 + 'em';
    div.style.right = 0 + 'em';
    div.onmousedown = callback;
    element.appendChild(div);
}


function selectComponent(element) {
    element.style.zIndex = 1000;

    deselectComponents();

    if (!element.classList.contains('selected')) {
        var l = parseInt(element.style.left.substring(0, element.style.left.length - 2));
        var t = parseInt(element.style.top.substring(0, element.style.top.length - 2));
        element.style.left = (l - 1) + 'px';
        element.style.top = (t - 1) + 'px';
        element.classList.add('selected');
    }

    if (element.classList.contains('wire')) {
        showDeleteSign(element, removeWire);
        showWireParams(element);
    } else if (element.classList.contains('switch')) {
        showDeleteSign(element, removeSwitch);
        showSwitchParams(element);
    } else if (element.classList.contains('bomb')) {
        showDeleteSign(element, removeBomb);
        showBombParams(element);
    } else if (element.classList.contains('bulb')) {
        showDeleteSign(element, removeBulb);
        showBulbParams(element);
    }
}

function showRulers(element) {
    if (element.classList.contains('wire')) {
        showWireConnections(element);
    } else if (element.classList.contains('switch')) {
        showSwitchConnections(element);
    } else if (element.classList.contains('bomb')) {
        showBombConnections(element);
    } else if (element.classList.contains('bulb')) {
        showBulbConnections(element);
    }
}

function deselectComponent(element) {
    for (var i = 0; i < element.childNodes.length; i++) {
        if (element.childNodes[i].classList.contains('delete-sign')) {
            element.removeChild(element.childNodes[i]);
            break;
        }
    }
    element.style.zIndex = 0;

    if (element.classList.contains('selected')) {
        var l = parseInt(element.style.left.substring(0, element.style.left.length - 2));
        var t = parseInt(element.style.top.substring(0, element.style.top.length - 2));
        element.style.left = (l + 1) + 'px';
        element.style.top = (t + 1) + 'px';

        element.classList.remove('selected');
    }
}

function deselectComponents() {
    clearCurrentParams();

    var i;
    for (i = 0; i < switches.length; i++) {
        deselectComponent(switches[i]);
    }

    for (i = 0; i < wires.length; i++) {
        deselectComponent(wires[i]);
    }

    for (i = 0; i < bulbs.length; i++) {
        deselectComponent(bulbs[i]);
    }

    for (i = 0; i < bombs.length; i++) {
        deselectComponent(bombs[i]);
    }
}